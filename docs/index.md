# Introduction to UWBCL 

*For full documentation follow [this link](https://dejanrupnik.bitbucket.io/#File:BaseStation.h).*



​     

## Connection

As soon as the Connection object is created, the port will be opened. 

```cpp
std::string ip = "1:1:1:1";
uint16_t port = 3993;
uwbcl::Connection connection = uwbcl::Connection::Tcp_Ip(ip, port);
```

​    

## Base Station 

```cpp
uint64_t timeout = 20;
uwbcl::BaseStation baseStation = uwbcl::BaseStation(connection, timeout);
bool baseStation_ping = baseStation.ping();
std::string baseStation_firmwareVersion_str = baseStation.firmwareVersion().str();
std::string baseStation_name = baseStation.name();
```



You can test results with:

```cpp
std::cout << "[BaseStation] Ping: " << (baseStation_ping ? "True" : "False") << std::endl;
std::cout << "[BaseStation] Firmware Version: " << baseStation_firmwareVersion_str << std::endl;
std::cout << "[BaseStation] Name: " << baseStation_name << std::endl << std::endl;
```



​    

## Node

### Creating a WirelessNode

```cpp
uwbcl::NodeAddress nodeAddress = 3863;
uint16_t idleTimeout = 1200;
uwbcl::WirelessNode wirelessNode = uwbcl::WirelessNode(nodeAddress, baseStation);
```



### Communicating with a Node 

Note: `status.cancel()` can be used to cancel Stop Node operation.

```cpp
uwbcl::SetToIdleStatus status = wirelessNode.setToIdle();
status.complete(idleTimeout);
uwbcl::PingResponse response = wirelessNode.ping();

bool response_success = response.success();
uwbcl::SetToIdleStatus::SetToIdleResult status_result = status.result();
uwbcl::Version nodeVer = wirelessNode.firmwareVersion();
std::string nodeVer_str = nodeVer.str();
std::string wirelessNode_name = wirelessNode.name();
WirelessTypes::Frequency wirelessNode_frequency = wirelessNode.frequency();
```



You can test results with:

```cpp
std::cout << "[WirelessNode] Success: " << (response_success ? "True" : "False") << std::endl;

switch (status_result)
{
    case uwbcl::SetToIdleStatus::setToIdleResult_success:
        std::cout << "[WirelessNode] SetToIdleResult: " << "Node is now in idle mode." << std::endl;
        break;
    
    case uwbcl::SetToIdleStatus::setToIdleResult_canceled:
		std::cout << "[WirelessNode] SetToIdleResult: " << "Set to Idle was canceled!" << std::endl;
		break;

	case uwbcl::SetToIdleStatus::setToIdleResult_failed:
		std::cout << "[WirelessNode] SetToIdleResult: " << "Set to Idle has failed!" << std::endl;
		break;
}

std::cout << "[WirelessNode] Firmware Version: " << nodeVer_str << std::endl;
std::cout << "[WirelessNode] Name: " << wirelessNode_name << std::endl;
std::cout << "[WirelessNode] Frequency: " << wirelessNode_frequency << std::endl << std::endl;
```



​    

## Node Configuration 

### Get current Config

```cpp
uwbcl::WirelessChannels allChannels = wirelessNode.features().channels();
uwbcl::ChannelMask activeChannels = wirelessNode.getActiveChannels();
```



Example: check currently active channels

```cpp
std::cout << "[ChannelMask] Active Channels: ";
int i = 1;
for (auto channel : activeChannels.getBitMask()._mask)
{
    if (channel == true) //check if active
        std::cout << "channel_" << i << " ";
	i++;
} std::cout << std::endl;
```



### Setting the Config

Set variables such as: ChannelMask, SamplingMode, 



Example: set Channel Mask

```cpp
uwbcl::ChannelMask activate;
uint8_t channelId = 6;
activate.enable(channelId, true); //set channel 6 to true

uwbcl::WirelessNodeConfig config;
config.activeChannels(activate);

/*Check channels*/
std::cout << "[WirelessNodeConfig] Active Channels: ";
i = 1;
for (auto channel : config.getActiveChannels().getBitMask()._mask)
{
    if (channel == true) //check if active
        std::cout << "channel_" << i << " ";
	i++;
} std::cout << std::endl;
```



Example: set SamplingMode

```cpp
uwbcl::WirelessTypes::SamplingMode samplingMode = uwbcl::WirelessTypes::SamplingMode::samplingMode_sync;
config.samplingMode(samplingMode);

/*Check sampling mode*/
std::cout << "[WirelessNodeConfig] Sampling Mode: ";
switch (config.getSamplingMode())
{
    case uwbcl::WirelessTypes::samplingMode_sync:
        std::cout << "samplingMode_sync";
        break;
    case uwbcl::WirelessTypes::samplingMode_nonSync:
        std::cout << "samplingMode_nonSync";
        break;
    default:
        std::cout << "Not configured";
} std::cout << std::endl;
```

### 

Example: set SampleRate

There are two different ways to get SampleRate:

1. By converting SamplingMode to SampleRate,

```cpp
   uwbcl::SampleRate sr_converted = uwbcl::SampleUtils::convertToSampleRate(wirelessNode.getSampleRate(samplingMode));
```

2. or initializing it using the sample rate and number of samples.

```cpp
   /*Gets all sample rates, supported by the Node.*/
uwbcl::WirelessTypes::WirelessSampleRates rates = wirelessNode.features().sampleRates(samplingMode);
uwbcl::SampleRate sr(uwbcl::SampleRate::rateType_hertz, 1024);
```

Apply values to WirelessNodeConfig: 

```cpp
config.sampleRate(sr.toWirelessSampleRate());
```



Check sample rate:

```cpp
/*Check sample rate*/
std::cout << "[WirelessNodeConfig] Wireless Sample Rate: " << config.getSampleRate() << " (check WirelessTypes::WirelessSampleRate for full value name)" << std::endl;
```



### Apply Config to Wireless Node

```cpp
uwbcl::ConfigIssues issues;
wirelessNode.verifyConfig(config, issues);
wirelessNode.applyConfig(config);
```

​    

## Sampling 

### Create Network

```cpp
uwbcl::SyncSamplingNetwork network = uwbcl::SyncSamplingNetwork(baseStation);
network.addNode(wirelessNode);
network.refresh();
```

### Check Network status

Make sure that network's bandwidth isn't more that 100% full.

```cpp
uwbcl::SyncNetworkInfo info = network.getNodeNetworkInfo(wirelessNode.nodeAddress());
info.percentBandwidth();
info.status();
network.percentBandwidth();
network.ok();
```

### Start Sampling (Synchronized or Non-Synchronized)

```cpp
uwbcl::SyncSamplingNetwork* network_ptr = &network;
network_ptr->startSampling(); //start sync sampling or ... 
uwbcl::WirelessNode* wirelessNode_ptr = &wirelessNode;
wirelessNode_ptr->startNonSyncSampling(); //... start async sampling 
```

​    

## Collecting Data

Collect data points from all of the Nodes in the given Network.

```cpp
uwbcl::DataSweeps sweeps = baseStation.getData();
for (uwbcl::DataSweep sweep : sweeps)
{
    sweep.samplingType();
	sweep.sampleRate().samplesPerSecond();
	sweep.nodeAddress();

	uwbcl::ChannelData data = sweep.data();
	for (const auto& dataPoint : data)
	{
		dataPoint.channelId();
		dataPoint.storedAs();
		dataPoint.as_double();
	}
}
	
status = wirelessNode.setToIdle();
status.complete(idleTimeout);
```

​    

## Devices

Access attached devices.

```cpp
uwbcl::Devices::DeviceList devicesList = uwbcl::Devices::listBaseStations();
for (const auto& d : devicesList)
{
	if (d.second.connectionType() == uwbcl::Connection::connectionType_serial)
	{
		//do something
	}
}
```



